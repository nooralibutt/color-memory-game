﻿/*
 * Board class is the backbone of the game.
 * It holds all the tiles, shuffle and initialize them and 
 * controls what happens on click of each tile and notify other classes 
 * on successfully match of tile, On failure to match of tile and on ending of game.
 * */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NAB
{
	public enum BoardState
	{
		IDLE,
		WAITING
	}

	public class Board : MonoBehaviour
	{
		public static event System.Action<Board> OnTileMatchSuccess;
		public static event System.Action<Board> OnTileMatchFail;
		public static event System.Action<Board> OnGameEnded;

		/// The game board consists of a 4x4 grid with 8 pairs of color cards
		private const int BOARD_SIZE = 4;

		/// The game pairs consists on half  board size
		private const int PAIRS_COUNT = BOARD_SIZE * BOARD_SIZE / 2;

		/// Brief Time delay on cards match and card fail match
		private const float TIME_DELAY = 1f;


		/// Pregenerated Tile Indexes
		private int[]tileIndexes = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};

		/// All the tiles in game
		public Transform[]tiles;

		/// Each tile would have a front visual Image to show
		public Sprite[]sprites;

		/// Each tile would have back visual Image to show
		public Sprite backSprite;

		/// Current State of board, either idle or in waiting state
		private BoardState mBoardState;

		/// Previously flipped Tile which match to be found
		private Tile previouslyFlippedTile;

		/// Total pairs completed so far
		private int pairsCompleted = 0;

		/// Use this for initialization
		void Start ()
		{
			populateBoard ();
		}

		public void OnEnable ()
		{
			Tile.OnTileFlipped += HandleOnTileFlipped;
		}

		public void OnDisable ()
		{
			Tile.OnTileFlipped -= HandleOnTileFlipped;
		}

		void HandleOnTileFlipped (Tile tile)
		{
			// If previouslyFlippedTile is null that means match process is just started
			if (previouslyFlippedTile == null) {
				previouslyFlippedTile = tile;
			} else {
				// Else check if it's a match then remove'em
				if (previouslyFlippedTile.tileIndex == tile.tileIndex) {

					// Play Match Success Sound
					SoundController.Instance.PlaySuccessFX();
					// Set Board State to waiting 
					mBoardState = BoardState.WAITING;
					// Remove cards after brief pause
					StartCoroutine (PauseAndRemoveCards (tile));
				} else {
					// Else if it's not a match then
					// Play Match Fail Sound
					SoundController.Instance.PlayFailFX();
					// Set Board State to waiting 
					mBoardState = BoardState.WAITING;
					// flip both of them to IDLE state after brief pause
					StartCoroutine (PauseAndSetCardsIdle (tile));
				}
			}
		}

		IEnumerator PauseAndSetCardsIdle (Tile tile)
		{
			yield return new WaitForSeconds (TIME_DELAY);

			if (OnTileMatchFail != null) {
				OnTileMatchFail(this);
			}

			tile.FlipTile (TileState.IDLE);
			previouslyFlippedTile.FlipTile (TileState.IDLE);

			// Reset Previously Flipped Tile
			previouslyFlippedTile = null;

			mBoardState = BoardState.IDLE;
		}

		IEnumerator PauseAndRemoveCards (Tile tile)
		{
			yield return new WaitForSeconds (TIME_DELAY);

			if (OnTileMatchSuccess != null) {
				OnTileMatchSuccess(this);
			}

			tile.RemoveMyself ();
			previouslyFlippedTile.RemoveMyself ();
			
			// Reset Previously Flipped Tile
			previouslyFlippedTile = null;
			
			mBoardState = BoardState.IDLE;

			pairsCompleted++;
			if (pairsCompleted >= PAIRS_COUNT) {
				EndGame();
			}
		}

		public void EndGame() {
			// Notify others
			if (OnGameEnded != null) {
				OnGameEnded (this);
			}

			// Play Game Complete Sound
			SoundController.Instance.PlayCompleteFX ();
		}

		/// Finally handling click on tile
		public void OnClickTile (Tile tile)
		{
			// If it hits on tile and board is not in waiting state
			if (mBoardState == BoardState.IDLE) {

				// Tile is fliping for the first time
				if (previouslyFlippedTile == null) {
					SoundController.Instance.PlayToggleFX();
				}

				// If there is no tile which is flipped previously
				// OR else if user did not clicked on same tile
				if (previouslyFlippedTile == null || tile != previouslyFlippedTile) {
					tile.FlipTile (TileState.FLIPPED);
				} 
			}
		}

		private void populateBoard ()
		{
			// Shuffling the pregenerated Indexes
			tileIndexes = ShuffleArray<int> (tileIndexes);
			
			// Initializing each individual sprites
			for (int i = 0; i < tiles.Length; i++) {
				int randomIndex = tileIndexes [i];
				tiles [i].GetComponent<Tile> ().InitTile (sprites [randomIndex], backSprite, randomIndex);
			}

			// Ready to take input
			mBoardState = BoardState.IDLE;
		}

		/// Shuffles the Generic Array
		private static T[] ShuffleArray<T> (T[] array)
		{
			System.Random r = new System.Random ();
			for (int i = array.Length; i > 0; i--) {
				int j = r.Next (i);
				T k = array [j];
				array [j] = array [i - 1];
				array [i - 1] = k;
			}
			return array;
		}
	}
}