﻿/*
 * Sound Controller controls the sound FX played throughout the game. 
 * It's a singleton instance which never destroys until user quits game.
 * */

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour {

	public AudioClip toggleClip;
	public AudioClip successClip;
	public AudioClip failClip;
	public AudioClip completeClip;
	public AudioClip buttonClip;

	private AudioSource audioSource;

	/// Static instance of Singleton Class which allows it to be accessed by any other script
	private static SoundController _instance = null;
	
	/// <summary>
	/// Gets the instance of the Singleton Class
	/// If there is not an instance, Singleton Class will generate a gameObject "MySingletonClass" in current scene.
	/// The instance game object will not be destroyed when load new scene.
	/// </summary>
	/// <value>The instance in the scene.</value>
	public static SoundController Instance {
		get {
			if (!_instance) {
				_instance = FindObjectOfType(typeof(SoundController)) as SoundController;
				
				if (!_instance) {
					var obj = new GameObject("SoundController");
					_instance = obj.AddComponent<SoundController>();
				} else {
					_instance.gameObject.name = "SoundController";
				}
			}
			return _instance;
		}
	}
	
	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// Awake is used to initialize any variables or game state before the game starts.
	/// Awake is called only once during the lifetime of the script instance.
	/// </summary>
	void Awake() {
		if (_instance == null) {
			_instance = this;
			// Sets this to not be destroyed when reloading scene
			DontDestroyOnLoad(_instance.gameObject);
		} else if (_instance != this) {
			// If there's any other object exist of this type delete it
			// as it's breaking our singleton pattern
			Destroy(gameObject);
		}

		audioSource = GetComponent<AudioSource> ();
	}

	private void PlayFX(AudioClip clip) {
		audioSource.PlayOneShot(clip);
	}

	public void PlayButtonFX() {
		PlayFX (buttonClip);
	}

	public void PlaySuccessFX() {
		PlayFX (successClip);
	}

	public void PlayFailFX() {
		PlayFX (failClip);
	}

	public void PlayCompleteFX() {
		PlayFX (completeClip);
	}

	public void PlayToggleFX() {
		PlayFX (toggleClip);
	}
}
