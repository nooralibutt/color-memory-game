﻿/*
 * Score Manager manages the score shown on game screen. It knows how much to gain on successfull match and lose on failure.
 * It also shows game over dialogue which prompts user for his name at the end of game.
 * */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NAB
{
	public class ScoreManager : MonoBehaviour
	{
		/// UI Dialogue to show loading when submiting scores
		public GameObject loadingDialogue;

		/// Points deficient on Mis-Match
		private const int MATCH_FAIL_POINTS = -1;
		/// Points Gain on Correct Match
		private const int MATCH_SUCCESS_POINTS = 2;

		/// UI Text reference to update current score
		public Text scoresText;

		public GameObject gameOverDialogue;
		public GameObject errorMessage;

		public GameObject highScoresButton;

		/// Holds the current score of the user
		private int currentScore;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// Awake is used to initialize any variables or game state before the game starts.
		/// Awake is called only once during the lifetime of the script instance.
		/// </summary>
		private void Awake() {
			// Initial Score
			currentScore = 0;
		}

		private void OnEnable ()
		{
			Board.OnTileMatchSuccess += HandleOnTileMatchSuccess;
			Board.OnTileMatchFail += HandleOnTileMatchFail;
			Board.OnGameEnded += HandleOnGameEnded;

			ParseController.OnScoreSubmitted += HandleOnScoreSubmitted;
		}


		private void OnDisable ()
		{
			Board.OnTileMatchSuccess -= HandleOnTileMatchSuccess;
			Board.OnTileMatchFail -= HandleOnTileMatchFail;
			Board.OnGameEnded -= HandleOnGameEnded;

			ParseController.OnScoreSubmitted -= HandleOnScoreSubmitted;
		}

		private void HandleOnScoreSubmitted (string obj)
		{
			loadingDialogue.SetActive (false);
		}

		private void HandleOnTileMatchFail (Board board)
		{
			currentScore += MATCH_FAIL_POINTS;
			UpdateScoreUI ();
		}

		private void HandleOnTileMatchSuccess (Board board)
		{
			currentScore += MATCH_SUCCESS_POINTS;
			UpdateScoreUI ();
		}

		private void HandleOnGameEnded (Board board)
		{
			gameOverDialogue.SetActive (true);
			highScoresButton.SetActive (false);
		}

		private void UpdateScoreUI() {
			scoresText.text = "Score: " + currentScore;
		}

		public void OnClickNameContinue(Text nameText) {
			// Play Button Sound
			SoundController.Instance.PlayButtonFX ();

			if (nameText.text.Length == 0) {
				errorMessage.SetActive(true);
			} else {
				gameOverDialogue.SetActive(false);
				loadingDialogue.SetActive (true);

				ParseController.Instance.SubmitScore(nameText.text, currentScore);
			}
		}
	}
}