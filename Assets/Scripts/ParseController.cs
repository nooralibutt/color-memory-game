﻿/*
 * Parse Controller is responsible for submiting and fetching scores from Network-Based database.
 * It's a singleton instance which never destroys until user quits game.
 * It notify's whenever it submits scores to web. All the comunications run on background without blocking the UI.
 * */

using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;

namespace NAB
{
	public class ParseController : MonoBehaviour
	{
		public static event System.Action<string> OnScoreSubmitted;

		/// Static instance of Singleton Class which allows it to be accessed by any other script
		private static ParseController _instance = null;
	
		/// <summary>
		/// Gets the instance of the Singleton Class
		/// If there is not an instance, Singleton Class will generate a gameObject "MySingletonClass" in current scene.
		/// The instance game object will not be destroyed when load new scene.
		/// </summary>
		/// <value>The instance in the scene.</value>
		public static ParseController Instance {
			get {
				if (!_instance) {
					_instance = FindObjectOfType (typeof(ParseController)) as ParseController;
				
					if (!_instance) {
						var obj = new GameObject ("ParseController");
						_instance = obj.AddComponent<ParseController> ();
					}
				}
				return _instance;
			}
		}
	
		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// Awake is used to initialize any variables or game state before the game starts.
		/// Awake is called only once during the lifetime of the script instance.
		/// </summary>
		void Awake ()
		{
			if (_instance == null) {
				_instance = this;
				// Sets this to not be destroyed when reloading scene
				DontDestroyOnLoad(_instance.gameObject);
			} else if (_instance != this) {
				// If there's any other object exist of this type delete it
				// as it's breaking our singleton pattern
				Destroy(gameObject);
			}

			// Registering the GameScore subclass of ParseObject:
			ParseObject.RegisterSubclass<GameScore> ();
		}

		public void SubmitScore (string name, int score)
		{
			GameScore gs = new GameScore ();
			gs.name = name;
			gs.score = score;

			StartCoroutine (SubmitScoreAsync (gs));
		}

		private IEnumerator SubmitScoreAsync (GameScore gameScore) {
			var queryTask = gameScore.SaveAsync ();

			while (!queryTask.IsCompleted) {
				yield return null;
			}

			string message = "";
			if (queryTask.IsFaulted) {
				message += "Score submission failed.";
			} else {
				message += "Score submission successfull.";
			}

			if (OnScoreSubmitted != null) {
				OnScoreSubmitted (message);
			}
		}

		public void GetScores (System.Action<List<GameScore>> OnGameScoresRecieved)
		{
			StartCoroutine (RetrieveScoresAsync (OnGameScoresRecieved));
		}

		private IEnumerator RetrieveScoresAsync (System.Action<List<GameScore>> OnGameScoresRecieved)
		{
			var queryTask = new ParseQuery<GameScore> ().OrderByDescending ("score").Limit (10).FindAsync ();
				
			while (!queryTask.IsCompleted) {
				yield return null;
			}

			List<GameScore> listOfGameScores = null;
			if (!queryTask.IsFaulted) {
				listOfGameScores = new List<GameScore> ();

				IEnumerable<GameScore> results = queryTask.Result;
				foreach (var item in results) {
					listOfGameScores.Add (item);
				}
			} else {
				print ("Parse error= " + queryTask.Exception.ToString ());
			}

			if (OnGameScoresRecieved != null) {
				OnGameScoresRecieved (listOfGameScores);
			}
		}
	}

	[ParseClassName("GameScore")]
	public class GameScore : ParseObject
	{
		[ParseFieldName("score")]
		public int score {
			get { return GetProperty<int> ("score"); }
			set { SetProperty<int> (value, "score"); }
		}

		[ParseFieldName("name")]
		public string name {
			get { return GetProperty<string> ("name"); }
			set { SetProperty<string> (value, "name"); }
		}
	}
}