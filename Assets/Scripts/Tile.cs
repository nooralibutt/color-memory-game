﻿/*
 * Tile class represent each card shown in UI.
 * Each tile is uniquely identified by an Index assigned to it.
 * It has a function to flip itself and it fires an event whenever a tile is flipped to front side.
 * */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace NAB
{
	public enum TileState
	{
		IDLE,
		FLIPPED
	}

	public class Tile : MonoBehaviour
	{
		/// If debugging mode is on, all the cards will be Shown to you to solve easily
		private bool isDebuggingModeOn = false;

		public static event System.Action<Tile> OnTileFlipped;

		/// <summary>
		/// Each tile is uniquely identified by an Index
		/// </summary>
		public int tileIndex = 0;

		/// <summary>
		/// The front sprite of tile
		/// </summary>
		private Sprite frontSprite;

		/// <summary>
		/// The back sprite of tile
		/// </summary>
		private Sprite backSprite;

		/// Initialize Tile to uniquely Identify
		public void InitTile (Sprite frontSprite, Sprite backSprite, int tileIndex)
		{
			// Initialing Instance variables
			this.tileIndex = tileIndex;
			this.frontSprite = frontSprite;
			this.backSprite = backSprite;

			GetComponent<Image> ().sprite = backSprite;

			if (isDebuggingModeOn) {
				GetComponent<Image> ().sprite = frontSprite;
				Debug.LogWarning ("Debugging Mode is On in Tile Class. Please turn it off");
			}

			transform.rotation = Quaternion.AngleAxis (360f, Vector3.up);
		}

		private TileState flipTileTo;
		public void FlipTile (TileState state)
		{
			flipTileTo = state;
				
			GetComponent<Animation> ().Play ();

			float lengthOfAnimation = 0.16f;
			Invoke ("flippingDone", lengthOfAnimation);
		}

		private void flippingDone() {
			Sprite sprite = backSprite;
			switch (flipTileTo) {
			case TileState.IDLE:
				sprite = backSprite;
				break;
			case TileState.FLIPPED:
				sprite = frontSprite;

				if (OnTileFlipped != null) {
					OnTileFlipped (this);
				}
				break;
			default:
				break;
			}
			
			// If debugging mode is on
			if (isDebuggingModeOn) {
				sprite = frontSprite;
				Debug.LogWarning ("Debugging Mode is On in Tile Class. Please turn it off");
			}
			
			Image image = GetComponent<Image> ();
			image.sprite = sprite;
		}

		public void RemoveMyself ()
		{
			Destroy (gameObject);
		}
	}
}