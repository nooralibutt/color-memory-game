﻿/*
 * Highscore Manager as name suggests manages highscore table.
 * It shows a list of highscores fetched from database residing on parse.
 * It communicates ParseController to fetch highscores using it's singleton instance
 * It also has a button to restart the game
 * */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace NAB
{
	public class HighscoreManager : MonoBehaviour
	{
		public RectTransform rowPrefab;

		public GameObject restartButton;
		public GameObject resumeButton;

		public GameObject highScoresDialogue;
		public GameObject touchBlocker;
		public GameObject titleRow;
		public Transform highscoresContainer;

		public Text dialogueMessage;

		private GameObject buttonToShowOnRecievingHighscores;

		void OnEnable() {
			ParseController.OnScoreSubmitted += HandleOnScoreSubmitted;
		}

		void OnDisable() {
			ParseController.OnScoreSubmitted -= HandleOnScoreSubmitted;
		}

		void HandleOnScoreSubmitted (string message)
		{
			touchBlocker.SetActive (true);
			highScoresDialogue.SetActive (true);
			
			dialogueMessage.text = message + "\nFetching Highscores ...";
			dialogueMessage.gameObject.SetActive (true);

			buttonToShowOnRecievingHighscores = restartButton;
			
			ParseController.Instance.GetScores (OnHighscoreRetrieved);
		}

		public void OnClickHighscores() {
			// Play Button Sound
			SoundController.Instance.PlayButtonFX ();

			touchBlocker.SetActive (true);
			highScoresDialogue.SetActive (true);

			dialogueMessage.text = "Loading Highscores ...";
			dialogueMessage.gameObject.SetActive (true);

			buttonToShowOnRecievingHighscores = resumeButton;

			ParseController.Instance.GetScores (OnHighscoreRetrieved);
		}

		private void OnHighscoreRetrieved(List<GameScore> listOfGameScore) {

			buttonToShowOnRecievingHighscores.SetActive (true);

			if (listOfGameScore == null) {
				dialogueMessage.text = "Please check your internet connection.";
			}
			else if (listOfGameScore.Count == 0) {
				dialogueMessage.text = "No Scores found.";
			} else {
				dialogueMessage.gameObject.SetActive (false);
				
				// Enable Title Bar
				titleRow.SetActive(true);
				
				// Populate Scores
				int rank = 1;
				foreach (var gamescore in listOfGameScore) {
					
					// Creating new row
					RectTransform rt = Instantiate(rowPrefab);
					rt.SetParent(highscoresContainer);
					rt.localScale = Vector3.one;
					
					// Populating Rank
					rt.GetChild(0).GetComponent<Text>().text = rank + "";
					
					// Populating Name
					rt.GetChild(1).GetComponent<Text>().text = gamescore.name;
					
					// Populating Score
					rt.GetChild(2).GetComponent<Text>().text = gamescore.score + "";
					
					rank++;
				}
			}
		}

		public void OnClickResume() {
			// Play Button Sound
			SoundController.Instance.PlayButtonFX ();

			touchBlocker.SetActive (false);
			highScoresDialogue.SetActive (false);
			resumeButton.SetActive (false);
			titleRow.SetActive(false);

			// Clear out previous scores
			foreach (Transform item in highscoresContainer) {
				Destroy(item.gameObject);
			}
		}

		public void OnClickRestart() {
			// Play Button Sound
			SoundController.Instance.PlayButtonFX ();

			Application.LoadLevel (0);
		}
	}
}